#define ana_tele_ev_tree_cxx
#include "ana_tele_ev_tree.h"
#include <TH2.h>
#include <TF1.h>
#include <TGraph.h>
#include <TLine.h>
#include <TStyle.h>
#include <TCanvas.h>

void ana_tele_ev_tree::Loop()
{
  if (fChain == 0) return;
  const Long64_t nentries = fChain->GetEntries();

  const Int_t nLayers = 4;
  const Int_t nSVXPerLayer = 4;
  const Int_t nStripsPerSVX = 128;
  const Int_t minADCCount = -1;//-1 means masked
  const Int_t maxADCCount = 256;

  gStyle->SetOptFit();
  gStyle->SetOptStat("eMinoru");
  gStyle->SetPalette(1);
  gStyle->SetStatStyle(0);

  TCanvas* c1 = new TCanvas();

  //Making histogram
  TFile* outfile = new TFile("pedestal.root","RECREATE");
  TH1F::SetDefaultSumw2();

  TH1F* h1_final_pedestal[nLayers][nSVXPerLayer];
  TH1F* h1_final_bad_strips[nLayers][nSVXPerLayer];
  TH1F* h1_final_masked_strips[nLayers][nSVXPerLayer];

  TGraph* gr_mean_pedestal_vs_event[nLayers][nSVXPerLayer];
  TH1F* h1_mean_pedestal[nLayers][nSVXPerLayer];//mean ADC of all channels calculated event-by-event
  TH1F* h1_pedestal[nLayers][nSVXPerLayer];
  TH1F* h1_mean_pedestal_vs_chip = new TH1F("h1_mean_pedestal_vs_chip","",nLayers*nSVXPerLayer,0,nLayers*nSVXPerLayer);
  TH1F* h1_pedestal_fitchi2overndf[nLayers][nSVXPerLayer];
  TH1F* h1_pedestal_fitchi2overndf_per_strip[nLayers][nSVXPerLayer];
  TH1D* h1_adc[nLayers][nSVXPerLayer][nStripsPerSVX];
  TH2F* h2_rawHit[nLayers][nSVXPerLayer];
  for(Int_t ilayer=0; ilayer<nLayers; ilayer++){
    for(Int_t isvx=0; isvx<nSVXPerLayer; isvx++){
      gr_mean_pedestal_vs_event[ilayer][isvx] = new TGraph();
      gr_mean_pedestal_vs_event[ilayer][isvx]->SetName(Form("gr_mean_pedestal_vs_event_%d_%d",ilayer,isvx));
      //      h1_mean_pedestal[ilayer][isvx] = new TH1F(Form("h1_mean_pedestal_%d_%d",ilayer,isvx),";<ADC>;#events",maxADCCount-minADCCount,minADCCount,maxADCCount);
      h1_mean_pedestal[ilayer][isvx] = new TH1F(Form("h1_mean_pedestal_%d_%d",ilayer,isvx),";<ADC>;#events",30,40,70);
      h1_pedestal[ilayer][isvx] = new TH1F(Form("h1_pedestal_%d_%d",ilayer,isvx),";Channel;Pedestal",30,40,70);
      h1_pedestal_fitchi2overndf[ilayer][isvx] = new TH1F(Form("h1_pedestal_fitchi2overndf_%d_%d",ilayer,isvx),";#chi^{2}/ndf;#events",3000,0,3000);
      h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx] = new TH1F(Form("h1_pedestal_fitchi2overndf_per_strip_%d_%d",ilayer,isvx),";Channel;#chi^{2}/ndf",nStripsPerSVX,0.-0.5,nStripsPerSVX-0.5);
      h2_rawHit[ilayer][isvx] = new TH2F(Form("h2_rawHit_%d_%d",ilayer,isvx),";Channel;ADC",nStripsPerSVX,0.-0.5,nStripsPerSVX-0.5,maxADCCount-minADCCount,minADCCount-0.5,maxADCCount-0.5);
      h1_final_pedestal[ilayer][isvx] = new TH1F(Form("h1_final_pedestal_%d_%d",ilayer,isvx),";Pedestal;Channel",nStripsPerSVX,0.-0.5,nStripsPerSVX-0.5);
      h1_final_bad_strips[ilayer][isvx] = new TH1F(Form("h1_final_bad_strips_%d_%d",ilayer,isvx),";Channel;",nStripsPerSVX,0.-0.5,nStripsPerSVX-0.5);
      h1_final_masked_strips[ilayer][isvx] = new TH1F(Form("h1_final_masked_strips_%d_%d",ilayer,isvx),";Channel;",nStripsPerSVX,0.-0.5,nStripsPerSVX-0.5);

//       for(Int_t istrip=0; istrip<nStripsPerSVX; istrip++){
//         h1_adc[ilayer][isvx][istrip] = new TH1F(Form("h1_adc_%d_%d_%d",ilayer,isvx,istrip),";ADC;#events",maxADCCount-minADCCount,minADCCount-0.5,maxADCCount-0.5);
//       }
    }
  }

  // Branch name...
  //    Int_t           Ev_Num;
  //    Int_t           Timestamp;
  //    Int_t           Status;
  //    Short_t         chipID[4][4];
  //    Short_t         pipeID[4][4];
  //    Short_t         hitnum[4][4];
  //    Short_t         adc[4][4][128];

  ////////////////////////////////////
  std::cout<<"Total number of events = "<<nentries<<std::endl;
  Long64_t nbytes = 0, nb = 0;
  for(Long64_t jentry=0; jentry<nentries;jentry++){
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;

    if(jentry%10000==0) std::cout<<"Processing events "<<jentry<<"/"<<nentries<<std::endl;
    //    if(jentry==10) break;

//     //Print out tree contents
//     std::cout<<"Event: "    <<jentry<<" ================ "<<std::endl;
//     std::cout<<"Ev_Num   : "<<Ev_Num<<std::endl;
//     std::cout<<"Timestamp: "<<Timestamp<<std::endl;
//     std::cout<<"Status   : "<<Status   <<std::endl;
//     for(Int_t ilayer=0; ilayer<nLayers; ilayer++){
//       for(Int_t isvx=0; isvx<nSVXPerLayer; isvx++){
//         std::cout<<"chipID["<<ilayer<<"]["<<isvx<<"] : " <<chipID[ilayer][isvx]<<std::endl;
//         std::cout<<"pipeID["<<ilayer<<"]["<<isvx<<"] : " <<pipeID[ilayer][isvx]<<std::endl;
//         std::cout<<"hitnum["<<ilayer<<"]["<<isvx<<"] : " <<hitnum[ilayer][isvx]<<std::endl;
//         for(Int_t istrip=0; istrip<nStripsPerSVX; istrip++){
//           std::cout<<"adc["<<ilayer<<"]["<<isvx<<"]["<<istrip<<"]: "<<adc[ilayer][isvx][istrip]<<std::endl;
//         }
//       }
//     }

    //Filling histograms
    Int_t nValidHits[nLayers][nSVXPerLayer] = {0};
    Double_t sumADC[nLayers][nSVXPerLayer] = {0};
    TH1F* h1_rawHit[nLayers][nSVXPerLayer];
    for(Int_t ilayer=0; ilayer<nLayers; ilayer++){
      for(Int_t isvx=0; isvx<nSVXPerLayer; isvx++){
        for(Int_t istrip=0; istrip<nStripsPerSVX; istrip++){
          if(adc[ilayer][isvx][istrip]>=0){
            h2_rawHit[ilayer][isvx]->Fill(istrip,adc[ilayer][isvx][istrip]);
          }
          if(0<=adc[ilayer][isvx][istrip] && adc[ilayer][isvx][istrip]<maxADCCount){
            nValidHits[ilayer][isvx]++;
            sumADC[ilayer][isvx]+=adc[ilayer][isvx][istrip];
          }
        }
      }
    }

    for(Int_t ilayer=0; ilayer<nLayers; ilayer++){
      for(Int_t isvx=0; isvx<nSVXPerLayer; isvx++){
        Double_t mean_pedestal = sumADC[ilayer][isvx]/nValidHits[ilayer][isvx];
        gr_mean_pedestal_vs_event[ilayer][isvx]->SetPoint(gr_mean_pedestal_vs_event[ilayer][isvx]->GetN(),jentry,mean_pedestal);
        h1_mean_pedestal[ilayer][isvx]->Fill(mean_pedestal);
      }
    }

//     //Showing event-by-event histogram
//     if(0<=jentry && jentry<10){
//       for(Int_t ilayer=0; ilayer<nLayers; ilayer++){
//         for(Int_t isvx=0; isvx<nSVXPerLayer; isvx++){
//           h1_rawHit[ilayer][isvx] = new TH1F(Form("h1_rawHit_%d_%d",ilayer,isvx),"",nStripsPerSVX,0.-0.5,nStripsPerSVX-0.5);
//           h1_rawHit[ilayer][isvx]->GetYaxis()->SetRangeUser(0.,maxADCCount+5);
//           for(Int_t istrip=0; istrip<nStripsPerSVX; istrip++){
//             h1_rawHit[ilayer][isvx]->SetBinContent(istrip+1,adc[ilayer][isvx][istrip]);
//           }
//           h1_rawHit[ilayer][isvx]->Draw("");
//           c1->Print(Form("figures/pedestal/eventdisplay/%s_eve%lld.pdf",h1_rawHit[ilayer][isvx]->GetName(),jentry));
//           h1_rawHit[ilayer][isvx]->Delete();
//         }
//       }
//     }

    // if (Cut(ientry) < 0) continue;
  }//End of event loop

  //////////////////////////////////////////////////
  //Post event loop treatment
  const int ndf_threshold=6;
  for(Int_t ilayer=0; ilayer<nLayers; ilayer++){
    for(Int_t isvx=0; isvx<nSVXPerLayer; isvx++){
      std::cout<<"Bin="<<nLayers*ilayer+isvx+1<<", Layer="<<ilayer<<", Chip="<<isvx<<std::endl;
      for(Int_t istrip=0; istrip<nStripsPerSVX; istrip++){
        h1_adc[ilayer][isvx][istrip] = h2_rawHit[ilayer][isvx]->ProjectionY(Form("h1_adc_%d_%d_%d",ilayer,isvx,istrip),istrip+1,istrip+1);
        std::cout<<"Bin="<<nLayers*ilayer+isvx+1<<", Layer="<<ilayer<<", Chip="<<isvx<<", Strip="<<istrip<<std::endl;
        h1_adc[ilayer][isvx][istrip]->Fit("gaus");
        if(h1_adc[ilayer][isvx][istrip]->GetFunction("gaus")!=NULL){
          std::cout<<h1_adc[ilayer][isvx][istrip]->GetFunction("gaus")->GetParameter(0)<<", "<<h1_adc[ilayer][isvx][istrip]->GetFunction("gaus")->GetParameter(1)<<", "<<h1_adc[ilayer][isvx][istrip]->GetFunction("gaus")->GetParameter(2)<<std::endl;;
          Double_t chi2 = h1_adc[ilayer][isvx][istrip]->GetFunction("gaus")->GetChisquare();
          Double_t ndf = h1_adc[ilayer][isvx][istrip]->GetFunction("gaus")->GetNDF();
          if(ndf>ndf_threshold){
            h1_pedestal_fitchi2overndf[ilayer][isvx]->Fill(chi2/ndf);
            h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->SetBinContent(istrip+1,chi2/ndf);
            h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->SetBinError(istrip+1,1.);
          }
        }
      }
    }
  }

  //Drawing histograms
  const Double_t badPixelThresholdSigma = 3.;

  TLine *vertical_threshold_for_fit_failed[nLayers][nSVXPerLayer];
  TLine *horizontal_threshold_for_fit_failed[nLayers][nSVXPerLayer];
  for(Int_t ilayer=0; ilayer<nLayers; ilayer++){
    for(Int_t isvx=0; isvx<nSVXPerLayer; isvx++){
      gr_mean_pedestal_vs_event[ilayer][isvx]->SetMarkerStyle(20);
      gr_mean_pedestal_vs_event[ilayer][isvx]->Draw("AP");
      gr_mean_pedestal_vs_event[ilayer][isvx]->GetXaxis()->SetTitle("Event");
      gr_mean_pedestal_vs_event[ilayer][isvx]->GetYaxis()->SetTitle("<ADC>");
      gr_mean_pedestal_vs_event[ilayer][isvx]->GetYaxis()->SetRangeUser(50,70);
      c1->Print(Form("figures/pedestal/%s.pdf",gr_mean_pedestal_vs_event[ilayer][isvx]->GetName()));

      h1_mean_pedestal[ilayer][isvx]->Draw("");
      c1->Print(Form("figures/pedestal/%s.pdf",h1_mean_pedestal[ilayer][isvx]->GetName()));

      h1_pedestal_fitchi2overndf[ilayer][isvx]->Draw();
      Double_t chi2_mean = h1_pedestal_fitchi2overndf[ilayer][isvx]->GetMean();
      Double_t chi2_rms = h1_pedestal_fitchi2overndf[ilayer][isvx]->GetRMS();
      vertical_threshold_for_fit_failed[ilayer][isvx] = new TLine(chi2_mean+badPixelThresholdSigma*chi2_rms,0.,chi2_mean+badPixelThresholdSigma*chi2_rms,h1_pedestal_fitchi2overndf[ilayer][isvx]->GetMaximum()*1.3);
      vertical_threshold_for_fit_failed[ilayer][isvx]->SetLineColor(kRed);
      vertical_threshold_for_fit_failed[ilayer][isvx]->SetLineStyle(kDotted);
      vertical_threshold_for_fit_failed[ilayer][isvx]->Draw();
      c1->Print(Form("figures/pedestal/%s.pdf",h1_pedestal_fitchi2overndf[ilayer][isvx]->GetName()));
      h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->Draw();
      horizontal_threshold_for_fit_failed[ilayer][isvx] = new TLine(1.-0.5,chi2_mean+badPixelThresholdSigma*chi2_rms,128-0.5,chi2_mean+badPixelThresholdSigma*chi2_rms);
      horizontal_threshold_for_fit_failed[ilayer][isvx]->SetLineColor(kRed);
      horizontal_threshold_for_fit_failed[ilayer][isvx]->SetLineStyle(kDotted);
      horizontal_threshold_for_fit_failed[ilayer][isvx]->Draw();
      //      h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->Fit("pol0");
      //      std::cout<<h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->GetEntries()<<", "<<h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->GetFunction("pol0")<<std::endl;
      //      std::cout<<h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->GetEntries()<<", "<<h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->GetFunction("pol0")<<std::endl;
      c1->Print(Form("figures/pedestal/%s.pdf",h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->GetName()));

      for(Int_t istrip=0; istrip<nStripsPerSVX; istrip++){
        h1_adc[ilayer][isvx][istrip]->GetXaxis()->SetRangeUser(30,90);
        h1_adc[ilayer][isvx][istrip]->Draw();
        c1->Print(Form("figures/pedestal/rawhit/%s.pdf",h1_adc[ilayer][isvx][istrip]->GetName()));
      }
    }
  }

  //////////////////////////////////////////////////
  //Quick analysis
  std::cout<<"Trying to detect strangely behaving strips. They will be stored in a root file to mask them."<<std::endl;
  Int_t nBadPixels = 0;
  for(Int_t ilayer=0; ilayer<nLayers; ilayer++){
    for(Int_t isvx=0; isvx<nSVXPerLayer; isvx++){
      Double_t chi2_mean = h1_pedestal_fitchi2overndf[ilayer][isvx]->GetMean();
      Double_t chi2_rms = h1_pedestal_fitchi2overndf[ilayer][isvx]->GetRMS();
      Int_t nValidHits = 0;
      Double_t mean_pedestal_of_good_strips = 0.;
      Double_t rms_pedestal_of_good_strips = 0.;
      for(Int_t istrip=0; istrip<nStripsPerSVX; istrip++){
        Double_t tmpMean = h1_adc[ilayer][isvx][istrip]->GetMean();
        Double_t tmpRms = h1_adc[ilayer][isvx][istrip]->GetRMS();
        h1_final_pedestal[ilayer][isvx]->SetBinContent(istrip+1,tmpMean);
        h1_final_pedestal[ilayer][isvx]->SetBinError(istrip+1,tmpRms);
        Double_t ndf = h1_adc[ilayer][isvx][istrip]->GetFunction("gaus")->GetNDF();
        Double_t tmpChi2 = h1_pedestal_fitchi2overndf_per_strip[ilayer][isvx]->GetBinContent(istrip+1);
        Bool_t notBad = true;
//         if(ilayer==1 and isvx==2 and istrip==29){
//           std::cout<<tmpMean<<", "<<tmpRms<<", "<<ndf<<", "<<tmpChi2<<", "<<chi2_mean<<", "<<chi2_rms<<std::endl;
//         }
        if(ndf>ndf_threshold && chi2_mean+badPixelThresholdSigma*chi2_rms<tmpChi2 && 100.<tmpChi2){
          nBadPixels++;
          std::cout<<nBadPixels<<"(LargeChi2)  , (Layer,Chip,Strip)=("<<ilayer<<","<<isvx<<","<<istrip<<"): Pedestal="<<tmpMean<<"+/-"<<tmpRms<<", Chi2="<<tmpChi2<<std::endl;
          //          h1_final_bad_strips[ilayer][isvx]->SetBinContent(istrip+1,1);
          h1_final_bad_strips[ilayer][isvx]->SetBinContent(istrip+1,1);
          notBad = false;
        }
        if(tmpRms>2.5){
          if(notBad){
            nBadPixels++;
            std::cout<<nBadPixels<<"(Noisy)      , (Layer,Chip,Strip)=("<<ilayer<<","<<isvx<<","<<istrip<<"): Pedestal="<<tmpMean<<"+/-"<<tmpRms<<", Chi2="<<tmpChi2<<std::endl;
            h1_final_bad_strips[ilayer][isvx]->SetBinContent(istrip+1,1);
            notBad = false;
          }
        }
        if(tmpMean<1){
          nBadPixels++;
          std::cout<<nBadPixels<<"(NoResponse) , (Layer,Chip,Strip)=("<<ilayer<<","<<isvx<<","<<istrip<<"): Pedestal="<<tmpMean<<"+/-"<<tmpRms<<", Chi2="<<tmpChi2<<std::endl;
          h1_final_bad_strips[ilayer][isvx]->SetBinContent(istrip+1,1);
          notBad = false;
        }
        if(tmpMean>250){
          nBadPixels++;
          std::cout<<nBadPixels<<"(ADCOverflow), (Layer,Chip,Strip)=("<<ilayer<<","<<isvx<<","<<istrip<<"): Pedestal="<<tmpMean<<"+/-"<<tmpRms<<", Chi2="<<tmpChi2<<std::endl;
          h1_final_masked_strips[ilayer][isvx]->SetBinContent(istrip+1,1);
          notBad = false;
        }
        if(notBad){
          nValidHits++;
          //          std::cout<<nValidHits<<"(Good), (Layer,Chip,Strip)=("<<ilayer<<","<<isvx<<","<<istrip<<"): "<<tmpMean<<"+/-"<<tmpRms<<std::endl;
          mean_pedestal_of_good_strips += tmpMean;
          rms_pedestal_of_good_strips += tmpRms;
        }
      }
      mean_pedestal_of_good_strips /= nValidHits;
      rms_pedestal_of_good_strips /= nValidHits;
      //      std::cout<<"Bin="<<nLayers*ilayer+isvx+1<<", "<<mean_pedestal_of_good_strips<<"+/-"<<rms_pedestal_of_good_strips<<std::endl;
      h1_mean_pedestal_vs_chip->SetBinContent(nLayers*ilayer+isvx+1,mean_pedestal_of_good_strips);
      h1_mean_pedestal_vs_chip->SetBinError(nLayers*ilayer+isvx+1,rms_pedestal_of_good_strips);

      h1_final_bad_strips[ilayer][isvx]->Draw();
      c1->Print(Form("figures/pedestal/%s.pdf",h1_final_bad_strips[ilayer][isvx]->GetName()));
      h1_final_masked_strips[ilayer][isvx]->Draw();
      c1->Print(Form("figures/pedestal/%s.pdf",h1_final_masked_strips[ilayer][isvx]->GetName()));

    }
  }

  h1_mean_pedestal_vs_chip->Draw();
  c1->Print(Form("figures/pedestal/%s.pdf",h1_mean_pedestal_vs_chip->GetName()));
  TLine* line_for_mean_pedestal[nLayers][nSVXPerLayer];
  for(Int_t ilayer=0; ilayer<nLayers; ilayer++){
    for(Int_t isvx=0; isvx<nSVXPerLayer; isvx++){
      Double_t mean_pedestal = h1_mean_pedestal_vs_chip->GetBinContent(nLayers*ilayer+isvx+1);
      Double_t rms_pedestal = h1_mean_pedestal_vs_chip->GetBinError(nLayers*ilayer+isvx+1);
      std::cout<<"Mean for this chip: Layer="<<ilayer<<", Chip="<<isvx<<", "<<mean_pedestal<<"+/-"<<rms_pedestal<<std::endl;

      h2_rawHit[ilayer][isvx]->Draw("colz");
      line_for_mean_pedestal[ilayer][isvx] = new TLine(-0.5,mean_pedestal,127.5,mean_pedestal);
      line_for_mean_pedestal[ilayer][isvx]->SetLineColor(kRed);
      line_for_mean_pedestal[ilayer][isvx]->SetLineStyle(kDotted);
      line_for_mean_pedestal[ilayer][isvx]->Draw();
      c1->Print(Form("figures/pedestal/%s.pdf",h2_rawHit[ilayer][isvx]->GetName()));

    }
  }

  outfile->Write();
  outfile->Close();

  delete c1;

}
