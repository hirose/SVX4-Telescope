//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Mar  1 01:41:17 2018 by ROOT version 6.12/06
// from TTree tele_ev_tree/Tele_EventTree
// found on file: ../data/run_00001033_tree.root
//////////////////////////////////////////////////////////

#ifndef ana_tele_ev_tree_h
#define ana_tele_ev_tree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ana_tele_ev_tree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           Ev_Num;
   Int_t           Timestamp;
   Int_t           Status;
   Short_t         chipID[4][4];
   Short_t         pipeID[4][4];
   Short_t         hitnum[4][4];
   Short_t         adc[4][4][128];

   // List of branches
   TBranch        *b_Ev_Num;   //!
   TBranch        *b_Timestamp;   //!
   TBranch        *b_Status;   //!
   TBranch        *b_chipID;   //!
   TBranch        *b_pipeID;   //!
   TBranch        *b_hitnum;   //!
   TBranch        *b_adc;   //!

   ana_tele_ev_tree(TTree *tree=0);
   virtual ~ana_tele_ev_tree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ana_tele_ev_tree_cxx
ana_tele_ev_tree::ana_tele_ev_tree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../data/run_00001033_tree.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../data/run_00001033_tree.root");
      }
      f->GetObject("tele_ev_tree",tree);

   }
   Init(tree);
}

ana_tele_ev_tree::~ana_tele_ev_tree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ana_tele_ev_tree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ana_tele_ev_tree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ana_tele_ev_tree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Ev_Num", &Ev_Num, &b_Ev_Num);
   fChain->SetBranchAddress("Timestamp", &Timestamp, &b_Timestamp);
   fChain->SetBranchAddress("Status", &Status, &b_Status);
   fChain->SetBranchAddress("chipID", chipID, &b_chipID);
   fChain->SetBranchAddress("pipeID", pipeID, &b_pipeID);
   fChain->SetBranchAddress("hitnum", hitnum, &b_hitnum);
   fChain->SetBranchAddress("adc", adc, &b_adc);
   Notify();
}

Bool_t ana_tele_ev_tree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ana_tele_ev_tree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ana_tele_ev_tree::Cut(Long64_t entry)
{
  std::cout<<"Entry: "<<entry<<std::endl;
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ana_tele_ev_tree_cxx
